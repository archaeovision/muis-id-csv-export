(function(){    
    
    function $ (selector, el) {
         if (!el) {el = document;}
         //return el.querySelectorAll(selector);
         // Note: the returned object is a NodeList.
         // If you'd like to convert it to a Array for convenience, use this instead:
         return Array.prototype.slice.call(el.querySelectorAll(selector));
    }
    
    function makeDocument(content) {      
        var x=window.open();
        x.document.open();
        x.document.write(content);
        x.document.close();
    }

    function initDownload(content) {
        var a = window.document.createElement('a');
        a.href = window.URL.createObjectURL(new Blob([content], {type: 'text/csv'}));
        a.download = 'muisist.csv';

        // Append anchor to body.
        document.body.appendChild(a)
        a.click();

        // Remove anchor from body
        document.body.removeChild(a)
    }

    var output = new Array();
    
    // table header
    var header = $('#sylemResultTableWR thead th');
    var number_label = header[1].textContent.replace(/^\s+|\s+$/g, '');
    var name_label = header[3].textContent.replace(/^\s+|\s+$/g, '');
    var muis_id_label = "MuIS ID";
    // "\uFEFF" makes Excel to open file n UTF8 mode
    output.push("\uFEFF"+number_label+';'+name_label+';'+muis_id_label);
    
    // table body
    var rows = $('#sylemResultTableWR tbody tr');
    //var output = new Array();
    for(var i = 0; i < rows.length; i++){
        var cell = $('td', rows[i]);
        var number = cell[1].textContent.replace(/^\s+|\s+$/g, '');
        var name = cell[3].textContent.replace(/^\s+|\s+$/g, '');
        var muis_id = $('a[href]', cell[7])[0]['href'].replace(/\D*&sp=.(\d*)&sp=[\w\W]*/g,'$1');
        output.push(number +';"'+name +'";'+muis_id);
    }
    
    // output as webpage in separate window
    //makeDocument(output.join("<br/>"));
    
    // output as csv file
    initDownload(output.join("\n"));
})();
