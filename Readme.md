# MuIS ID eksport #

Tegemist on Chrome laiendusega, mis lisab nupukese tegumireale. Nupuke käivitab ekspordi MuIS sülemite lehel. Eksport-faili tulevad sissekanded, mis näha ühel lehel. 

Kui CSV faili avada, siis automaatselt jaguneb see tulpadeks siis kui arvuti regiooniseadetes on määratud eradajaks semikoolon.